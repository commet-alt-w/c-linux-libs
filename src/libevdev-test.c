#include "libevdev-test.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <errno.h>
#include <poll.h>
#include <libevdev-1.0/libevdev/libevdev.h>

#define INPUTPATH "/dev/input/event0"
#define FFLAGS O_RDONLY | O_NONBLOCK
#define LDEVFLAGS LIBEVDEV_READ_FLAG_NORMAL

static void
print_event(struct input_event *ev) {
  switch(ev->type) {
  case EV_SYN:
    printf("EVENT: time %ld.%ld, %s\n",
           ev->input_event_sec,
           ev->input_event_usec,
           libevdev_event_type_get_name(ev->type));
    break;
  default:
    printf("EVENT: time %ld.%ld, type: %d (%s), code: %d (%s), value: %d\n",
           ev->input_event_sec,
           ev->input_event_usec,
           ev->type,
           libevdev_event_type_get_name(ev->type),
           ev->code,
           libevdev_event_code_get_name(ev->type, ev->code),
           ev->value);
    break;
  }
}

static int
process_event(int rc, struct input_event *ev) {
  switch(rc) {
  case LIBEVDEV_READ_STATUS_SYNC: // SYN_DROPPED event
    printf("DROPPED\n");
    // loop while recieving dropped events
    while (rc == LIBEVDEV_READ_STATUS_SYNC) {
      printf("SYNC: ");
      // print event info
      print_event(ev);
    }
    printf("RE-SYNCED\n");
    break;
  case LIBEVDEV_READ_STATUS_SUCCESS: // read successful
    // print event info
    print_event(ev);
    // check for esc key being pressed
    if (ev->type == EV_KEY && ev->code == KEY_ESC) {
      printf("ESCAPE KEY PRESSED\n");
      return 1;
    }
    break;
  }
  return 0;
}

static void
print_device_info(struct libevdev *ldev) {
  printf("device name: %s\n", libevdev_get_name(ldev));
  printf("vendor id: %d\n", libevdev_get_id_vendor(ldev));
  printf("product id: %d\n", libevdev_get_id_product(ldev));
  printf("physical location: %s\n", libevdev_get_phys(ldev));
  printf("unique identifier: %s\n", libevdev_get_uniq(ldev));
}

void libevdev_test() {
  int fd = -1, rc = -1, got_esc = -1, ready = -1;
  struct libevdev *ldev;
  struct pollfd *pfd;
  struct input_event *ev;

  // open device
  fd = open(INPUTPATH, FFLAGS);
  // init libevdev on device
  rc = libevdev_new_from_fd(fd, &ldev);

  // check for errors
  if (rc < 0) {
    fprintf(stderr, "cannot init libevdev\n");
    fprintf(stderr, "ERROR: %s\n", strerror(-rc));
    exit(EXIT_FAILURE);
  }

  // init pollfd
  pfd = calloc(1, sizeof(struct pollfd));
  pfd[0].fd = fd;
  pfd[0].events = POLLIN;

  // init input event
  ev = calloc(1, sizeof(struct input_event));

  // print device info
  print_device_info(ldev);

  // let user know to press esc key
  printf("press <ESC> key to exit\n");
  
  // loop for device input events while reads successful
  // and user did not press ESC key
  do {
    // poll for input
    ready = poll(pfd, 1, -1);
    
    // check for errors
    if (ready == -1) {
      printf("cannot poll for input\n");
      printf("ERROR: %s", strerror(errno));
      exit(EXIT_FAILURE);
    }

    // check poll result
    if (pfd[0].revents & POLLIN) {
      // get input event
      rc = libevdev_next_event(ldev, LDEVFLAGS, ev);

      // process input event
      got_esc = process_event(rc, ev);
    }
    // zero out input event
    memset(ev, 0, sizeof(struct input_event));
  } while ((rc == LIBEVDEV_READ_STATUS_SYNC ||
            rc == LIBEVDEV_READ_STATUS_SUCCESS ||
            rc == -EAGAIN) &&
           got_esc != 1);

  // free input event
  free(ev);
  // free pollfd
  free(pfd);
  // free libevdev device
  libevdev_free(ldev);
  // close file descriptor
  close(fd);
}
