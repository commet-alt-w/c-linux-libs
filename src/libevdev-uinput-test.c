#include "libevdev-uinput-test.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <libevdev/libevdev-uinput.h>

#define UIFD LIBEVDEV_UINPUT_OPEN_MANAGED

static void
mouse_move_err_check(int err) {
  if (err != 0) {
    printf("error moving mouse\n");
    printf("ERROR: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
}

void libevdev_uinput_test() {
  int err;
  struct libevdev *dev;
  struct libevdev_uinput *uidev;

  // create libevdev mouse device
  dev = libevdev_new();
  libevdev_set_name(dev, "kbdm mouse");
  libevdev_enable_event_type(dev, EV_REL);
  libevdev_enable_event_code(dev, EV_REL, REL_X, NULL);
  libevdev_enable_event_code(dev, EV_REL, REL_Y, NULL);
  libevdev_enable_event_type(dev, EV_KEY);
  libevdev_enable_event_code(dev, EV_KEY, BTN_LEFT, NULL);

  // init libevdev uinput device
  err = libevdev_uinput_create_from_device(dev, UIFD, &uidev);

  // check for errors
  if (err != 0) {
    printf("cannot create libevdev uinput device\n");
    printf("ERROR: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // move mouse diagonally 5 units per axis in a loop 50 times
  for (int i = 0; i < 50; i++) {
    // 5 units horizontally
    err = libevdev_uinput_write_event(uidev, EV_REL, REL_X, 5);
    mouse_move_err_check(err);
    // 5 units vertically
    err = libevdev_uinput_write_event(uidev, EV_REL, REL_Y, 5);
    mouse_move_err_check(err);
    // report event
    err = libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    mouse_move_err_check(err);
    // sleep for 15000 microseconds
    usleep(15000);
  }

  // free memory
  libevdev_uinput_destroy(uidev);
  libevdev_free(dev);
}
