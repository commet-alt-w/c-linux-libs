#include "pciutils-test.h"
#include "hidapi-test.h"
#include "libevdev-test.h"
#include "libudev-test.h"
#include "queue-test.h"
#include "libevdev-uinput-test.h"
#include "libconfig-test.h"
#include <stdlib.h> 
#include <stdio.h>
#include <syslog.h>

int main() {
  printf("testing linux libraries\n");

  /* printf("\npciutils\n"); */
  /* pciutils_test(); */

  /* printf("\nhidapi\n"); */
  /* hidapi_test(); */

  /* printf("\nlibudev\n"); */
  /* libudev_test(); */
  
  /* printf("\nlibevdev\n"); */
  /* libevdev_test(); */

  /* printf("\nlibbsd sys/queue.h\n"); */
  /* queue_test(); */

  /* printf("\nlibevdev uinput test\n"); */
  /* libevdev_uinput_test(); */

  printf("\nlibconfig test\n");
  libconfig_test();

  exit(EXIT_SUCCESS);
}
