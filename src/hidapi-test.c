#include "hidapi-test.h"
#include <stdlib.h>
#include <stdio.h>
#include <wchar.h>
#include <hidapi/hidapi.h>

void hidapi_test() {
  struct hid_device_info *hids;
  struct hid_device_info *hid;

  hid_init();
  hids = hid_enumerate(0, 0);

  for (hid = hids; hid; hid = hid->next) {
    printf("found device: %ls\n", hid->product_string);
    printf("manufacturer string: %ls\n", hid->manufacturer_string);
    printf("device path: %s\n", hid->path);
  }
  
  hid_free_enumeration(hids);
  hid_exit();       
}
