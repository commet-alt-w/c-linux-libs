#include "pciutils-test.h"
#include <stdlib.h>
#include <stdio.h>
#include <pci/pci.h>

#define BUFFERSIZE 1024

void pciutils_test() {
  struct pci_access *pa;
  struct pci_dev *pdev;
  int pciflags = PCI_FILL_IDENT | PCI_FILL_BASES | PCI_FILL_CLASS;
  char * const buffer = malloc(BUFFERSIZE);

  pa = pci_alloc();
  pci_init(pa);
  pci_scan_bus(pa);

  for (pdev = pa->devices; pdev; pdev = pdev->next) {
    pci_fill_info(pdev, pciflags);
    pci_lookup_name(pa, buffer, BUFFERSIZE,
                    PCI_LOOKUP_DEVICE,
                    pdev->vendor_id,
                    pdev->device_id);
    printf("found pci device: %s\n", buffer);
  }

  pci_cleanup(pa);
  free(buffer);
}
