#include "libudev-test.h"
#include <stdlib.h>
#include <stdio.h>
#include <libudev.h>

#define TARGETSUBSYS "input"
#define TARGETPROPERTY "ID_INPUT_KEYBOARD"
#define TARGETPROPVAL "1"

static void
print_udev_device_properties(struct udev_device *pudevd) {
  struct udev_list_entry *pudevle;
  struct udev_list_entry *pudevlenext;
  pudevle = udev_device_get_properties_list_entry(pudevd);
  udev_list_entry_foreach(pudevlenext, pudevle) {
    const char *name;
    const char *value;
    name = udev_list_entry_get_name(pudevlenext);
    value = udev_list_entry_get_value(pudevlenext);
    printf("\tproperty name: %s\n", name);
    printf("\tproperty value: %s\n", value);
  }
}

static void
print_udev_device_info(struct udev *pudev,
                       struct udev_list_entry *pudevle) {
  // get and print device name
  const char *devicename = udev_list_entry_get_name(pudevle);
  printf("found device: %s\n", devicename);
  // get udev device from syspath (devicename)
  struct udev_device *pudevd;
  pudevd = udev_device_new_from_syspath(pudev, devicename);
  // get udev device information and print
  const char *devtype = udev_device_get_devtype(pudevd);
  const char *devsubsys = udev_device_get_subsystem(pudevd);
  const char *devpath = udev_device_get_devpath(pudevd);
  const char *sysname = udev_device_get_sysname(pudevd);
  printf("\tdevice type: %s\n", devtype);
  printf("\tdevice subsystem: %s\n", devsubsys);
  printf("\tdevice path: %s\n", devpath);
  printf("\tdevice sysname: %s\n", sysname);
  // print device properties
  print_udev_device_properties(pudevd);
  // free memory
  udev_device_unref(pudevd);
}

void libudev_test() {
  int enumr = 0;
  struct udev *pudev;
  struct udev_enumerate *pudeve;
  struct udev_enumerate *pudevef;
  struct udev_enumerate *pudevefp;
  struct udev_list_entry *pudevle;
  struct udev_list_entry *pudevlenext;

  // init udev and udev_enumerate
  pudev = udev_new();
  pudeve = udev_enumerate_new(pudev);

  printf("ALL DEVICES...\n");

  // scan for devices
  udev_enumerate_scan_devices(pudeve);

  // get list entries
  pudevle = udev_enumerate_get_list_entry(pudeve);

  // iterate through all devices
  udev_list_entry_foreach(pudevlenext, pudevle) {
    print_udev_device_info(pudev, pudevlenext);
  }

  printf("DEVICES FILTERED BY SUBSYSTEM...\n");

  // get devices by filtering subsystem
  pudevef = udev_enumerate_new(pudev);
  enumr = udev_enumerate_add_match_subsystem(pudevef, TARGETSUBSYS);

  if (enumr > -1) {
    udev_enumerate_scan_devices(pudevef);
    pudevle = udev_enumerate_get_list_entry(pudevef);

    // iterate filtered devices
    udev_list_entry_foreach(pudevlenext, pudevle) {
      print_udev_device_info(pudev, pudevlenext);
    }
  }

  printf("DEVICES FILTERED BY PROPERTY...\n");

  // get devices by filtering properties
  pudevefp = udev_enumerate_new(pudev);
  enumr = udev_enumerate_add_match_property(pudevefp, TARGETPROPERTY, TARGETPROPVAL);

  if (enumr > -1) {
    udev_enumerate_scan_devices(pudevefp);
    pudevle = udev_enumerate_get_list_entry(pudevefp);

    udev_list_entry_foreach(pudevlenext, pudevle) {
      print_udev_device_info(pudev, pudevlenext);
    }
  }

  // free memory
  udev_enumerate_unref(pudevefp);
  udev_enumerate_unref(pudevef);
  udev_enumerate_unref(pudeve);
  udev_unref(pudev);
}
