#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libconfig.h>

#define CONF_FILE "conf/c-linux-libs.conf"
#define OPTION_NAME "name"

void libconfig_test(void) {
  // init config
  config_t *conf = calloc(1, sizeof(config_t));
  config_init(conf);

  // get config option
  int result = 0;
  result = config_read_file(conf, CONF_FILE);

  // check for error
  if (result == CONFIG_FALSE) {
    fprintf(stderr, "cannot read config file\n");
    printf("ERROR: %s\n", strerror(errno));
  } else {
    printf("config file read\n");
  }

  // get option from config file
  const char *value = NULL;
  config_lookup_string(conf, OPTION_NAME, &value);

  printf("value: %s\n", value);
  
  // destroy config
  config_destroy(conf);
  // free memory
  free(conf);
}
