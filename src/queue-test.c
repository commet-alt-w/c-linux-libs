#include <stdlib.h>
#include <stdio.h>
#include <sys/queue.h>

// data type with singly linked queue declaration
typedef struct data {
  int d;
  STAILQ_ENTRY(data) items;
} data_t;

// struct declaration which defines head item
STAILQ_HEAD(stailq_head, data);

/*
 * print_head_remove
 *
 * iterate queue printing the head item and removing it
 */
static void print_head_remove(struct stailq_head *head) {
  while(STAILQ_EMPTY(head) == 0) {
    // get first item
    data_t *dp = NULL;
    dp = STAILQ_FIRST(head);
    // remove head from queue
    STAILQ_REMOVE_HEAD(head, items);
    // print item
    printf("data: %d\n", dp->d);
  }
}

void queue_test(void) {
  // init data
  data_t *d1 = calloc(1, sizeof(data_t));
  data_t *d2 = calloc(1, sizeof(data_t));
  data_t *d3 = calloc(1, sizeof(data_t));

  d1->d = 1;
  d2->d = 2;
  d3->d = 3;

  // init head item for queue
  struct stailq_head *head = calloc(1, sizeof(struct stailq_head));
  STAILQ_INIT(head);

  // check if list is empty
  if (STAILQ_EMPTY(head) != 0) {
    printf("data_t queue is empty\n");
    // add first item
    STAILQ_INSERT_HEAD(head, d3, items);
  }

  // add rest items at end of list
  STAILQ_INSERT_TAIL(head, d2, items);
  STAILQ_INSERT_TAIL(head, d1, items);

  // print items and remove for queue
  print_head_remove(head);
  
  // free memory
  free(d1);
  free(d2);
  free(d3);
  free(head);
}
