# c-linux-libs

testing code using c linux libraries

## requirements

1. pciutils-dev
   - `pci/pci.h`
1. hidapi-dev
   - `hidapi/hidapi.h`
1. libevdev-dev
   - `libevdev/libevdev.h`
1. eudev-dev
   - `libudev.h`
1. libbsd-dev
   - `sys/queue.h`
1. libconfig
   - `libconfig.h`

## setup

- `c-linux-libs.c`
  - uncomment tests you want to run
- `libevdev-uinput-test.c`
  - needs root permissions
  - need `/dev/uinput`
    - `$ modprobe uinput`

## building

- `$ make`

## running

- `$ ./build/release/c-linux-libs`
  - will need root permissions for libevdev uniput test

## license

[gnu gpl v3](/license)
